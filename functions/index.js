let functions = require('firebase-functions')
let admin = require('firebase-admin')
let shared = require('google-cloud-1-shared')

let region = process.env.REGION || 'europe-west2'

admin.initializeApp()

let storage = admin.storage()

exports.helloWorld = functions
  .region(region)
  .https.onRequest((request, response) => {
    response.send('hello world!')
  })

exports.onUserImageDelete = functions
  .region(region)
  .firestore.document('notes/{email}/images/{imageId}')
  .onDelete(async (snapshot, context) => {
    let data = snapshot.data()
    let fileName = shared.getStorageUserImageName(
      context.params.email,
      data.timestamp,
      data.name
    )
    return storage
      .bucket()
      .file(`notes_storage/${fileName}`)
      .delete()
  })
